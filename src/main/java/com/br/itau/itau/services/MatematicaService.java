package com.br.itau.itau.services;

import com.br.itau.itau.models.Matematica;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class MatematicaService {

    public int soma(List<Integer> numeros){
        int resultado = 0;
        for(int numero: numeros){
            resultado+=numero;
        }
        return resultado;
    }

    public Integer subtrai(List<Integer> numeros) {
        int resultado = numeros.get(0);
        for(int i=1; i<numeros.size();i++){
            resultado-=numeros.get(i);
        }
        return resultado;
    }

    public Integer multiplicar(List<Integer> numeros) {
        int resultado = 1;
        for(int numero: numeros){
            resultado *=numero;
        }
        return resultado;
    }

    public Integer dividir(List<Integer> numeros) {
        int resultado = numeros.get(0) / numeros.get(1);
        return resultado;
    }

    public void validacao(String tipo,List<Integer> numeros){
        for(int numero: numeros){
            if(numero<0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Não é permitido enviar números negativos");
            }
        }
        if(tipo.equals("divisao")){
            if(numeros.isEmpty()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessário enviar pelo menos dois numeros");
            }else if(numeros.size()>2){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Não é permitido enviar mais de dois números");
            }else if(numeros.contains(0)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Não é permitido dividir números por zero");
            }
        }else {
            if (numeros.isEmpty() || numeros.size() < 2) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário enviar pelo menos dois numeros");
            }
        }
    }

}
