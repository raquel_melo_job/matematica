package com.br.itau.itau.controller;

import com.br.itau.itau.models.Matematica;
import com.br.itau.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        matematicaService.validacao("soma",numeros);
        return matematicaService.soma(numeros);
    }

    @PostMapping("subtracao")
    public Integer subtrair(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        Collections.sort(numeros, Collections.reverseOrder());
        matematicaService.validacao("subtracao",numeros);
        return matematicaService.subtrai(numeros);
    }

    @PostMapping("multiplicacao")
    public Integer multiplicar(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        matematicaService.validacao("multiplicacao",numeros);
        return matematicaService.multiplicar(numeros);
    }

    @PostMapping("divisao")
    public Integer dividir(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        Collections.sort(numeros, Collections.reverseOrder());
        matematicaService.validacao("divisao",numeros);
        return matematicaService.dividir(numeros);
    }
}
