package com.br.itau.itau.models;

import java.util.List;

//padrao BEAN para tornar uma classe serializavel
//construtor vazio + getset
public class Matematica {

    private List<Integer> numeros;

    public Matematica() {
    }

    public Matematica(List<Integer> numeros) {
        this.numeros = numeros;
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
